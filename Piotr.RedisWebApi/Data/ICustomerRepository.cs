﻿using System;
using System.Collections.Generic;
using Piotr.RedisWebApi.Models;

namespace Piotr.RedisWebApi.Data
{
    public interface ICustomerRepository
    {
        IList<Customer> GetAll();
        Customer Get(Guid id);
        Customer Store(Customer customer);
    }
}